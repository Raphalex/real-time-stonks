using Godot;
using System;

#nullable enable

using OrderQueue =
System.Collections.Generic.Queue<RealTimeStonks.Server.Order>;

namespace RealTimeStonks.Server
{
  /** <summary> This class represents a general entity (could be for example a
   * building or a unit). </summary>
   * <remarks> Some attributes that could be considered private are made into
   * public read-only properties for display purposes (we might want to display
   * the current status of an entity, like if it's currently executing an order
   * or not). </remarks>
   */
  public class Entity : Node2D
  {
    /** Order queue: orders received by the entity are stored in this
     * queue and get pulled out when the entity is idle. */
    private OrderQueue _orderQueue = new OrderQueue();

    /** <summary> The order that is currently being executed. (null means no
     * order is currently being executed) </summary> */
    protected Order? CurrentOrder = null;

    /** <summary> Boolean to check wether or not the unit is currently executing
     * an order. </summary> */
    public bool ExecutingOrder { get; private set; } = false;

    /** <summary> This method is the way to pass an order to this entity. It
     * essentially enqueues the order in the entity's private order queue
     * </summary>
     * <param name="order"> The order to issue </param>
     */
    public void IssueOrder(Order order)
    {
      _orderQueue.Enqueue(order);
      /* If we were not executing any order, we should trigger the execution of
         the received order immediately */
      if (CurrentOrder is null)
      {
        ExecuteNextOrder();
      }
    }

    /** <summary> Method to pull out the next order in the queue and execute it.
     * </summary> */
    protected void ExecuteNextOrder()
    {
      /* In every case, if the entity calls to fetch the next order, it means
        the last one is completed, so we set the current order to null */
      CurrentOrder = null;
      if (_orderQueue.Count > 0)
      {
        // This is a double dispatch pattern : we need to cast both the order
        // and the entity itself to their dynamic types 
        dynamic order = _orderQueue.Dequeue();
        // Set current order
        CurrentOrder = order;
        ((dynamic)this).Execute(order);
      }
    }

    /** <summary> Entities executing orders is based on a double dispatch 
    * pattern : the orders are specialized and the entities as well. The orders
    contain all of the data needed to execute them, and it is up to the entity
    to implement the behaviour of the order execution. The Execute method is the concrete implementation of the execution of the order.
    
    <param name="order"> The order to execute (the type must inherit
    <c>Order</c>) </param */
    public virtual void Execute(Order order)
    {
      // Do nothing when receiving a general order in this class
    }
  }
}