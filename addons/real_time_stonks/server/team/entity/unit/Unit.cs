using Godot;
using System;
using System.Collections.Generic;

/* Type aliases for the map and positions */
using Map = Godot.Navigation2D;
using Position = Godot.Vector2;

namespace RealTimeStonks.Server
{
  /** <summary> Default template for a unit. This class tries to expand the
   * abstract <c>Entity</c> class to provide basic functionnality regarding
   * units in most RTSs. </summary> */
  public class Unit : Entity
  {
    [Export]
    public float Speed { get; private set; } = 10f;
    /** <summary> Implementation of the behaviour of the unit when issued a
     * <c>MoveOrder</c>. </summary> */

    /* A reference to the map of the game (a Godot Navigation2D node) */
    private Map _map;

    /** <summary> Public property that's used to set the _map attribute. of the
     * Unit. This attribute is used for the Unit to move around the map using
     * pathfinding */
    public Map Map
    {
      get
      {
        return _map;
      }
      set
      {
        _map = value;
      }
    }

    /* A queue of positions used to follow the path of the Unit */
    private Queue<Position> _nextPositions = new Queue<Position>();

    public override void _Process(float delta)
    {
      // Always call the base process function
      base._Process(delta);
      // If the position queue is not empty, move to the next position
      if (_nextPositions.Count > 0)
      {
        handleMovement(delta);
      }
    }

    /** <summary> Execute a move order. </summary> */
    public void Execute(MoveOrder order)
    {
      // We simply compute the path with the Nav node and enqueue the positions
      Position endPosition = new Position(order.Target.x, order.Target.y);
      _nextPositions =
      // Don't forget to translate coordinates to local coordinates of the map
      new Queue<Position>(
        _map.GetSimplePath(_map.ToLocal(GlobalTransform.origin),
        _map.ToLocal(endPosition)));
    }

    /** <summary> Handle the unit's movement. </summary>
      <param name="delta"> Seconds passed since last frame </param>
    */
    private void handleMovement(float delta)
    {
      // Compute direction of movement
      Position direction = _map.ToGlobal(_nextPositions.Peek()) -
      GlobalTransform.origin;
      // Always multiply by the delta
      float stepSize = delta * Speed;
      // Translate the coordinates back to Global
      GlobalTranslate(direction.Normalized() * stepSize);
      // When we get to the target position, remove it from the queue
      if (direction.Length() < stepSize)
      {
        _nextPositions.Dequeue();
        /* If this call just emptied the position queue and we were currently 
        executing a Move Order, it means we just finished this move order and need
        to fetch the next order */
        if (_nextPositions.Count == 0 && CurrentOrder is MoveOrder)
        {
          ExecuteNextOrder();
        }
      }
    }
  }
}