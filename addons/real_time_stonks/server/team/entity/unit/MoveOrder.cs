using Godot;
using System;

namespace RealTimeStonks.Server
{
  /** <summary> Basic move order. This class contains a target (2D) position for
   * the unit to move to. </summary> */
  public class MoveOrder : Order
  {
    public Vector2 Target { get; private set; }

    /** <summary> Constructor for the MoveOrder class. </summary>
    <param name="target"> Target coordinate for the unit to move to </param>
    */
    public MoveOrder(Vector2 target)
    {
      Target = target;
    }
  }
}