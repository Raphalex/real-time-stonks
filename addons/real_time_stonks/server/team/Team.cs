using Godot;
using System;

/** <summary> Simple type alias for a collection of server-side entities
 * </summary> */
using EntityVector = System.Collections.Generic.List<RealTimeStonks.Server.Entity>;

namespace RealTimeStonks.Server
{
  [Tool]
  /** <summary> A server-side team. This Team class regroups all of the entities
   * that belong to one team. </summary> */
  public class Team : Node
  {
    // List of entities that belong to this team
    private EntityVector _entities = new EntityVector();

    /** <summary> This is a readonly property to allow the client-side nodes to
     * fetch info about the server-side entities contained by this team
     * </summary> */
    public EntityVector Entities
    {
      get
      {
        // For new return full entities, later we will need the vision filter
        // and so on...
        return _entities;
      }
    }

    public override void _Ready()
    {
      // TODO :
      /* Remove this as this is a temporary solution used for testing
       purposes : we fetch every Entity node that is a child of this node
       (this is used for little examples or tests that place the units of
       entities directly in the editor) */
      var children = GetChildren();
      foreach (dynamic child in children)
      {
        if (child is Entity)
        {
          _entities.Add(child);
        }
      }
    }
  }
}