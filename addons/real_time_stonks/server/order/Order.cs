using Godot;
using System;

namespace RealTimeStonks.Server
{
  /** <summary> Classes inheriting from <c>Order</c> encode the data needed for
   * an order to take place. For exemple, one could imagine a <c>MoveOrder</c>
   * class inheriting from <c>Order</c> and containing a target position for a
   * unit to move to </summary> */
  public abstract class Order : Godot.Object
  {
  }
}