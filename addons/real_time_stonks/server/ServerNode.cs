using Godot;
using System;
using System.Linq;

/* Type alias for the map which is currently a Navigation node */
using Map = Godot.Navigation2D;

namespace RealTimeStonks.Server
{
  [Tool]
  /** <summary> "God" game object. This object contains the map node mesh (used
   * on the server-side) and the teams playing in the game. </summary> */
  public class ServerNode : Node
  {
    private NodePath _mapPath;
    private Map _map;

    /** <summary> NodePath that points to a Navigation node. This is a property
     * used to update the configuration warning and it actually refers to the
     * <c>_mapPath</c> private attribute. </summary> */
    [Export]
    public NodePath MapPath
    {
      get
      {
        return _mapPath;
      }
      set
      {
        _mapPath = value;
        // Update the config warning when the node path is set
        UpdateConfigurationWarning();
      }
    }

    private NodePath[] _teamPaths;
    private Team[] _teams;

    /** <summary> NodePath array that points to the Team nodes. This is a
     * property used to update the configuration warning and it actually refers
     * to the <c>_teamPath</c> private attribut. </summary> */
    [Export]
    public NodePath[] TeamPaths
    {
      get
      {
        return _teamPaths;
      }
      set
      {
        _teamPaths = value;
        UpdateConfigurationWarning();
      }
    }

    public override void _Ready()
    {
      _map = (Map)GetNode(_mapPath);
      // Initialize _teams array
      _teams = new Team[_teamPaths.Length];
      foreach (var (path, index) in _teamPaths.Select((path, index) => (path, index)))
      {
        _teams[index] = (Team)GetNode(path);
      }
    }

    /* Display configuration warning for this node */
    public override string _GetConfigurationWarning()
    {
      // Initialize config warning string
      string configWarning = "";
      // Case where the Navigation node is not set properly
      if (GetNodeOrNull<Map>(_mapPath) is null)
      {
        configWarning += "The MapPath property needs to be set to the valid path of a Navigation2D node.\n";
      }
      // Case where the Team nodes array is not set properly
      // Foreach with enumeration of the iterable
      foreach (var (path, index) in _teamPaths.Select((path, index) => (path, index)))
      {
        { /* Check custom node type for each NodePath in the Teams array */
          Node teamNode = GetNodeOrNull<Node>(path);
          if (teamNode is null) {
            configWarning += String.Format("The path specified in the TeamPaths property at index {0} is invalid.\n", index);
            break;
          }
          if (!(teamNode is Team))
          {
            configWarning += String.Format("The node pointed by the NodePath at index {0} of the TeamPaths property must be of type Team.\n", index);
          }
        }
      }
      return configWarning;
    }

    public Team[] GetGameState()
    {
      // Vision filter goes here
      return _teams;
    }

    public int GetTeamCount()
    {
      return _teams.Length;
    }
  }
}