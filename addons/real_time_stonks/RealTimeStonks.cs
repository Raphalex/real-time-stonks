using Godot;
using System;
#nullable enable

/** <summary> Quick type redefinition for simplification.
In order, the strings defined : the name of the node, the name of
the base (parent) node, a path to the script, and a path to the texture
(image to be displayed in the editor)
</summary> */
using NodeDeclaration = System.Tuple<string, string, string, string?>;

/** <summary> Entry point class for the Real Time Stonks Godot plugin,
it essentially registers the different nodes defined by the plugin. </summary>
*/

namespace RealTimeStonks
{
  [Tool]
  public class RealTimeStonks : EditorPlugin
  {
    private const string _pluginName = "Real Time Stonks";
    private const string _pluginDir = "real_time_stonks/";
    private const string _pluginPath = "res://addons/" + _pluginDir;

    /** <summary> List declared nodes here. </summary> */
    private NodeDeclaration[] _declaredNodes = {
    // Server nodes
    new NodeDeclaration("Entity", "Node2D", "server/team/entity/Entity.cs",
    null),
    new NodeDeclaration("Unit", "Node2D", "server/team/entity/unit/Unit.cs",
    null),
    new NodeDeclaration("Team", "Node", "server/team/Team.cs", null),
    new NodeDeclaration("ServerNode", "Node", "server/ServerNode.cs", null),
    
    // Client nodes
    new NodeDeclaration("ClientNode", "Node", "client/ClientNode.cs", null),
    new NodeDeclaration("ClientTeam", "Node", "client/team/Team.cs", null),
    new NodeDeclaration("ClientEntity", "Spatial",
    "client/team/entity/Entity.cs", null)
  };

    // Executes when the plugin is loaded
    public override void _EnterTree()
    {
      /* Run through each declared node and add the corresponding custom type to
      the engine */
      foreach (NodeDeclaration declaredNode in _declaredNodes)
      {
        // Texture can be passed as null, so do a simple check
        Texture? tex;
        if (declaredNode.Item4 is null)
          tex = null;
        else
          tex = GD.Load<Texture>(declaredNode.Item4);
        AddCustomType(declaredNode.Item1, declaredNode.Item2,
        GD.Load<Script>(_pluginPath + declaredNode.Item3),
        tex);
        GD.Print(_pluginName, ": Successfully registered custom node \"",
        declaredNode.Item1, "\"");
      }
    }

    // Executes when the plugin is unloaded
    public override void _ExitTree()
    {
      /* Run through each declared node and remove it */
      foreach (NodeDeclaration declaredNode in _declaredNodes)
      {
        RemoveCustomType(declaredNode.Item1);
        GD.Print(_pluginName, ": Successfully removed custom node \"",
        declaredNode.Item1, "\"");
      }
    }
  }
}