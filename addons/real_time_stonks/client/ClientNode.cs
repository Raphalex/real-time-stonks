using Godot;
using System;
using System.Linq;
using RealTimeStonks.Server;

namespace RealTimeStonks.Client
{
  /** <summary> General client node : this node orchestrates the rendering
   * (client) side of the plugin. </summary> */
  public class ClientNode : Node
  {
    // Path to the server (in the tree)
    private NodePath _serverPath;

    // A reference to the server
    private ServerNode _server;

    /** <summary> Public property to allow setting the path to the server node 
    in the editor. </summary> */
    [Export]
    public NodePath ServerPath
    {
      get
      {
        return _serverPath;
      }
      set
      {
        _serverPath = value;
      }
    }

    // An array of teams
    private Client.Team[] _teams;

    public override void _Ready()
    {
      _server = (ServerNode)GetNode<Node>(_serverPath);
      /* Initialize client-side teams : we fetch the info for
         each team from the server and initialize our array accordingly.
      */
      _teams = new Client.Team[_server.GetTeamCount()];
      Server.Team[] serverTeams = _server.GetGameState();
      for (int i = 0; i < _server.GetTeamCount(); ++i)
      {
        _teams[i] = new Client.Team(serverTeams[i]);
        AddChild(_teams[i]);
      }
    }
  }
}