using Godot;
using System;

namespace RealTimeStonks.Client
{
  /** <summary> Client-side entity. This class is the client
  equivalent of the server-side entity class. This is used purely
  for rendering purposes : this class contains a readlonly reference
  to the corresponding server-side entity in order to fetch game information
  to render. </summary>*/
  public class Entity : Spatial
  {
    // Readonly reference to the server-side entity
    private readonly Server.Entity _serverEntity;

    /** <summary> Client-side entity constructor. </summary>
        <param name="serverEntity"> The server-side entity that corresponds to
        this client-side entity. The entity uses this reference to get correct
        rendering info. </param> */
    public Entity(Server.Entity serverEntity)
    {
      _serverEntity = serverEntity;
    }

    // Empty constructor to prevent Godot from being angry
    public Entity() { }

    public override void _Ready()
    {
      // As a test, spawn a cube to visualize the unit.
      // TODO: This is a temporary solution, remove it later
      var cube = new CSGBox();
      AddChild(cube);
    }
    public override void _Process(float delta)
    {
      /* Will call the method to get needed/available information from the
         server node later */

      /* Get the position of the node server side and use this info to render
       this node correctly.*/
      Vector2 pos2D = _serverEntity.Transform.origin;
      Translation = new Vector3(pos2D.x, 0f, pos2D.y);
    }
  }
}