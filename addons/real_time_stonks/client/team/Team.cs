using Godot;
using System;

/** <summary> A simple type alias for a vector (C# List) of client-side entities
 * </summary> */
using EntityVector =
System.Collections.Generic.List<RealTimeStonks.Client.Entity>;

namespace RealTimeStonks.Client
{

  /** <summary> Client-side team. This is the counterpart to the server-side
   * team. It uses information fetched from the server-side team to render
   * appropriately.</summary> */
  public class Team : Node
  {
    // A readonly reference to the serverTeam
    private readonly Server.Team _serverTeam;
    // List of entities in this team
    private EntityVector _entities = new EntityVector();

    /** <summary> Constructor for the client-side team. This takes a reference 
    to the server-side team as an argument. </summary>
    <param name="serverTeam"> The server-side team this client-side team is the
    rendering of </param>
    */
    public Team(Server.Team serverTeam)
    {
      _serverTeam = serverTeam;
      /* This code is used to initialize the list of entities. 
         In real use case of this plugin, there shouldn't be any unit present.
         This initialization step is mostly used for testing and example
         purposes. */
      var serverEntities = _serverTeam.Entities;
      foreach (Server.Entity serverEntity in serverEntities)
      {
        Entity entity = new Entity(serverEntity);
        _entities.Add(entity);
        AddChild(entity);
      }
    }

    // Empty constructor to make Godot happy
    public Team() { }
  }
}