using Godot;
using System;
using RealTimeStonks.Server;

/* Test class for the Game Integration test */
public class ServerIntegration : Node
{
  public override void _Ready()
  {
    // Get the Unit node we want to send an order to
    Unit unit = (Unit)FindNode("Unit", true, true);
    // Get the navigation node
    Navigation2D map = (Navigation2D)FindNode("Navigation2D", true, true);
    unit.Map = map;
    unit.IssueOrder(new MoveOrder(new Vector2(23f, 16f)));
  }
}